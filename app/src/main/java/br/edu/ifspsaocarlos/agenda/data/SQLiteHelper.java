package br.edu.ifspsaocarlos.agenda.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class SQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "agenda.db";
    static final String DATABASE_TABLE = "contatos";
    static final String KEY_ID = "id";
    static final String KEY_NAME = "nome";
    static final String KEY_FONE = "fone";
    static final String KEY_FONE_2 = "fone_2";
    static final String KEY_EMAIL = "email";
    static final String KEY_FAVORITO = "favorito";
    static final String KEY_DIA_MES_ANIVERSARIO = "dia_mes_aniversario";
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_CREATE = "CREATE TABLE " + DATABASE_TABLE + " (" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_NAME + " TEXT NOT NULL, " +
            KEY_FONE + " TEXT, " +
            KEY_EMAIL + " TEXT, " +
            KEY_FAVORITO + " INTEGER, " +
            KEY_FONE_2 + " TEXT, " +
            KEY_DIA_MES_ANIVERSARIO + " TEXT);";

    SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        switch (newVersion) {
            case 1:
                Log.i("Contato", "Atualizando para a versao 1");
                database.execSQL("CREATE TABLE " + DATABASE_TABLE + " (" +
                        KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        KEY_NAME + " TEXT NOT NULL, " +
                        KEY_FONE + " TEXT, " +
                        KEY_EMAIL + " TEXT);");
            case 2:
                Log.i("Contato", "Atualizando para a versao 2");
                database.execSQL("ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN " + KEY_FAVORITO + " INTEGER;");
            case 3:
                Log.i("Contato", "Atualizando DB para a versao 3");
                database.execSQL("ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN " + KEY_FONE_2 + " TEXT;");
            case 4:
                Log.i("Contato", "Atualizando para a versão 4");
                database.execSQL("ALTER TABLE " + DATABASE_TABLE + " ADD COLUMN " + KEY_DIA_MES_ANIVERSARIO + " TEXT;");
        }
    }
}

