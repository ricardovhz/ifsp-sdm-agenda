package br.edu.ifspsaocarlos.agenda.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import br.edu.ifspsaocarlos.agenda.model.Contato;
import br.edu.ifspsaocarlos.agenda.R;

import java.util.List;


public class ContatoAdapter extends RecyclerView.Adapter<ContatoAdapter.ContatoViewHolder> {

    private List<Contato> contatos;
    private Context context;

    private static ItemClickListener clickListener;


    public ContatoAdapter(List<Contato> contatos, Context context) {
        this.contatos = contatos;
        this.context = context;
    }

    @Override
    public ContatoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contato_celula, parent, false);
        return new ContatoViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ContatoViewHolder holder, int position) {
        Contato contato = contatos.get(position);
        holder.nome.setText(contato.getNome());
        holder.imgFavorito.setImageResource(contatos.get(position).getFavorito() ? R.drawable.favorito : R.drawable.favoritonao);
    }

    @Override
    public int getItemCount() {
        return contatos.size();
    }


    public void setClickListener(ItemClickListener itemClickListener) {
        clickListener = itemClickListener;
    }


    public class ContatoViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final TextView nome;
        final ImageView imgFavorito;

        ContatoViewHolder(View view) {
            super(view);
            nome = (TextView) view.findViewById(R.id.nome);
            imgFavorito = view.findViewById(R.id.img_favorito);
            imgFavorito.setOnClickListener(this);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                if (view.getId() == R.id.img_favorito) {
                    Contato c = contatos.get(getAdapterPosition());
                    if (c.getFavorito()) {
                        imgFavorito.setImageResource(R.drawable.favoritonao);
                    } else {
                        imgFavorito.setImageResource(R.drawable.favorito);
                    }
                    clickListener.onFavoritoClick(getAdapterPosition());
                } else {
                    clickListener.onItemClick(getAdapterPosition());
                }
            }
        }
    }


    public interface ItemClickListener {
        void onItemClick(int position);

        void onFavoritoClick(int position);
    }

}


