package br.edu.ifspsaocarlos.agenda.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.edu.ifspsaocarlos.agenda.model.Contato;


public class ContatoDAO {
    private SQLiteDatabase database;
    private SQLiteHelper dbHelper;

    private static String[] COLUMNS = new String[]{SQLiteHelper.KEY_ID,
            SQLiteHelper.KEY_NAME,
            SQLiteHelper.KEY_FONE,
            SQLiteHelper.KEY_FONE_2,
            SQLiteHelper.KEY_EMAIL,
            SQLiteHelper.KEY_FAVORITO,
            SQLiteHelper.KEY_DIA_MES_ANIVERSARIO};

    public ContatoDAO(Context context) {
        this.dbHelper = new SQLiteHelper(context);
    }

    public List<Contato> buscaTodosContatos(boolean onlyFavorited) {
        return buscaContato(null, onlyFavorited);
    }

    public List<Contato> buscaContato(String termo, boolean onlyFavorited) {
        database = dbHelper.getReadableDatabase();
        List<Contato> contatos = new ArrayList<>();

        Cursor cursor;

        StringBuilder where = new StringBuilder();
        List<String> argWhere = new ArrayList<>();

        if (onlyFavorited) {
            where.append(SQLiteHelper.KEY_FAVORITO + " = ?");
            argWhere.add(onlyFavorited ? "1" : "0");
        }

        if (termo != null) {
            where.append((onlyFavorited ? " and " : "") +
                    " (" + SQLiteHelper.KEY_NAME + " like ? or " + SQLiteHelper.KEY_EMAIL + " like ?)");

            argWhere.add(termo + "%");
            argWhere.add("%" + termo + "%");
        }

        cursor = database.query(SQLiteHelper.DATABASE_TABLE, COLUMNS,
                where.toString(), argWhere.toArray(new String[0]),
                null, null, SQLiteHelper.KEY_NAME);

        while (cursor.moveToNext()) {
            contatos.add(cursorToContato(cursor));
        }
        cursor.close();

        database.close();
        return contatos;
    }

    public void salvaContato(Contato c) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.KEY_NAME, c.getNome());
        values.put(SQLiteHelper.KEY_FONE, c.getFone());
        values.put(SQLiteHelper.KEY_FONE_2, c.getFone2());
        values.put(SQLiteHelper.KEY_EMAIL, c.getEmail());
        values.put(SQLiteHelper.KEY_FAVORITO, (c.getFavorito() ? 1 : 0));

        String diaMesAniversario = c.getDiaMesAniversario();
        if (!validarDiaMesAniversario(diaMesAniversario)) {
            throw new IllegalArgumentException("Formato deve ser \"dd/MM\"");
        }
        values.put(SQLiteHelper.KEY_DIA_MES_ANIVERSARIO, c.getDiaMesAniversario());

        if (c.getId() > 0)
            database.update(SQLiteHelper.DATABASE_TABLE, values, SQLiteHelper.KEY_ID + "="
                    + c.getId(), null);
        else
            database.insert(SQLiteHelper.DATABASE_TABLE, null, values);

        database.close();
    }

    private boolean validarDiaMesAniversario(String diaMesAniversario) {
        try {
            if (diaMesAniversario == null || diaMesAniversario.trim().isEmpty())
                return true;

            if (!diaMesAniversario.contains("/"))
                return false;

            String[] splitted = diaMesAniversario.split("/");
            int dia = Integer.parseInt(splitted[0]);
            int mes = Integer.parseInt(splitted[1]);

            if (!(dia >= 1 && dia <= 31))
                return false;

            if (!(mes >= 1 && mes <= 12))
                return false;

            return true;
        } catch (Exception e) {
            Log.e("Contato", e.getMessage(), e);
        }
        return false;
    }


    public void apagaContato(Contato c) {
        database = dbHelper.getWritableDatabase();
        database.delete(SQLiteHelper.DATABASE_TABLE, SQLiteHelper.KEY_ID + "="
                + c.getId(), null);
        database.close();
    }

    private Contato cursorToContato(Cursor cursor) {
        Contato contato = new Contato();
        contato.setId(cursor.getInt(0));
        contato.setNome(cursor.getString(1));
        contato.setFone(cursor.getString(2));
        contato.setFone2(cursor.getString(3));
        contato.setEmail(cursor.getString(4));
        contato.setFavorito(cursor.getInt(5) == 0 ? Boolean.FALSE : Boolean.TRUE);
        contato.setDiaMesAniversario(cursor.getString(6));
        return contato;
    }

    public void toggleFavoritarContato(Contato contato) {
        database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(SQLiteHelper.KEY_FAVORITO, contato.getFavorito() ? 0 : 1);
        database.update(SQLiteHelper.DATABASE_TABLE, values, SQLiteHelper.KEY_ID + "=" + contato.getId(), null);
        database.close();
        contato.setFavorito(contato.getFavorito() ? Boolean.FALSE : Boolean.TRUE);
    }
}
